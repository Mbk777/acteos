$( document ).ready(function() {
var name = $('#name');
var address = $('#address');
var city = $('#city');
var dob = $('#dob');
var civstart = $('#CS');
var phone = $('#phone');
var email = $('#email');
var pic = $('#photo');
var educationDate = $('#eduDate');
var educationDesc = $('#eduDesc');
var profdate = $('#profdate');
var profDesc = $('#profDesc');
      $.ajax({  
          url: 'cvs.json', 
          dataType: 'json',
          success: function (items) { 
            name.append(items.cvs[0].cv.persData.name);
            address.append(items.cvs[0].cv.persData.Address);
            city.append(items.cvs[0].cv.persData.City);
            dob.append("<label> Date of Birth: "+items.cvs[0].cv.persData.DateofBirth+"</label>");
            civstart.append("<label> Civil status: " + items.cvs[0].cv.persData.CivState + "</label>");
            phone.append("<label> Phone: " + items.cvs[0].cv.persData.Phone + "</label>");
            email.append("<label> email: " + "<a href='mailto:"+ items.cvs[0].cv.persData.eMail + "'>" + items.cvs[0].cv.persData.eMail+  "</a></label>");
            pic.append("<img src='" + items.cvs[0].cv.persData.Pictures + "'>");
            for(var i=0 ; i<items.cvs[0].cv.Education.length;i++){
                educationDate.append("<div class='educationDate'> " + items.cvs[0].cv.Education[i].EduDate + "</div> <br/> ");
                educationDesc.append("<div class='educationDesc'> " +items.cvs[0].cv.Education[i].EduDesc + "</div> <br/> ");
            }
            for(var j=0 ; j<items.cvs[0].cv.ProfExp.length;j++){
                profdate.append("<div class='professionDate'> " +items.cvs[0].cv.ProfExp[j].ExpDateStart + " - " + items.cvs[0].cv.ProfExp[j].ExpDateEnd + "</div> <br/> " );
                profDesc.append("<div class='professionDesc'> " +items.cvs[0].cv.ProfExp[j].Company + " <br/> " + items.cvs[0].cv.ProfExp[j].ExpDesc1 + " and " + items.cvs[0].cv.ProfExp[j].ExpDesc2 + "</div> <br/> ") ; 
            }
            
            
          },
          error: function(){
              alert('error on load cvs.json');
            }
      });

   

    /////////////////////////////////////////////////////////////////////////////////////

    ;(function($) {
        $.fn.toJSON = function() {
            var $elements = {}; 
            var $form = $(this);
            $form.find('select').each(function(){
              var name = $(this).attr('name')
              
              if(name){
                var $value;
               
                  $value = $(this).val()
                
                $elements[$(this).attr('name')] = $value
              }
            });
            return JSON.stringify( $elements )
        };
        $.fn.fromJSON = function(json_string) {
            var $form = $(this)
            var data = JSON.parse(json_string)
            $.each(data, function(key, value) {
              var $elem = $('[name="'+key+'"]', $form)
              var type = $elem.first().attr('type')
              if(type == 'radio'){
                $('[name="'+key+'"][value="'+value+'"]').prop('checked', true)
              } else if(type == 'checkbox' && (value == true || value == 'true')){
                $('[name="'+key+'"]').prop('checked', true)
              } else {
                $elem.val(value)
              }
            })
        };
    }( jQuery ));
    
    //
    // DEMO CODE
    // 
    $(document).ready(function(){
       $("#_save").on('click', function(){
         console.log("Saving form data...")
         var data = $("form#myForm").toJSON()
         console.log(data);
         localStorage['form_data'] = data;
         
         return false;
       })
       
       $("#_load").on('click', function(){
         if(localStorage['form_data']){
           console.log("Loading form data...")
           console.log(JSON.parse(localStorage['form_data']))
           $("form#myForm").fromJSON(localStorage['form_data'])
         } else {
           console.log("Error: Save some data first")
         }
         
         return false;
       })
    });
    
    });